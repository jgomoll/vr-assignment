#!/usr/bin/python

import avango.daemon
import os
import sys

# functions
def init_pst_tracking():

  # create instance of DTrack
  pst = avango.daemon.DTrack()
  pst.port = "5020" # PST port
  
  pst.stations[1] = avango.daemon.Station('tracking-pst-head')
  pst.stations[2] = avango.daemon.Station('tracking-pst-augustpointer')
  pst.stations[3] = avango.daemon.Station('tracking-pst-whitemousepointer')
  
  device_list.append(pst)
  
  print "PST Tracking started!"


def init_lcd_wall_tracking():

  # create instance of DTrack
  _dtrack = avango.daemon.DTrack()
  _dtrack.port = "5000" # ART port at LCD wall
  
  # head tracking
  #_dtrack.stations[20] = avango.daemon.Station('tracking-lcd-head1') # passive glasses
  #_dtrack.stations[19] = avango.daemon.Station('tracking-lcd-head2')
  
  _dtrack.stations[4] = avango.daemon.Station('tracking-lcd-head1') # active glasses
  _dtrack.stations[3] = avango.daemon.Station('tracking-lcd-head2') # active glasses
  #_dtrack.stations[6] = avango.daemon.Station('tracking-lcd-camera') # camera shutter
  
  # device tracking
  _dtrack.stations[13] 	= avango.daemon.Station('tracking-lcd-pointer3') # August 1

  '''  
  _dtrack.stations[7] 	= avango.daemon.Station('tracking-spheron-old')
  _dtrack.stations[2] 	= avango.daemon.Station('tracking-lcd-pointer1') # green pointer (stable)
  _dtrack.stations[1] 	= avango.daemon.Station('tracking-lcd-pointer2') # blue pointer (unstable)
  #_dtrack.stations[9]		= avango.daemon.Station('tracking-lcd-wim1') # LHT 1
  _dtrack.stations[9]		= avango.daemon.Station('tracking-lcd-cube') # LHT 1
  #_dtrack.stations[10]	= avango.daemon.Station('tracking-lcd-cube') # tapping cube
  _dtrack.stations[11]	= avango.daemon.Station('tracking-lcd-portal1') # portal_cam1
  _dtrack.stations[5]		= avango.daemon.Station('tracking-lcd-portal2') # portal_cam2
  '''

  device_list.append(_dtrack)
  print "ART Tracking started at LCD WALL"



def init_spacemouse():

	_string = os.popen("/opt/avango/vr_application_lib/tools/list-ev -s | grep \"3Dconnexion SpaceNavigator\" | sed -e \'s/\"//g\'  | cut -d\" \" -f4").read()

	if len(_string) == 0:
		_string = os.popen("/opt/avango/vr_application_lib/tools/list-ev -s | grep \"3Dconnexion SpaceTraveler USB\" | sed -e \'s/\"//g\'  | cut -d\" \" -f4").read()

	_string = _string.split()
	if len(_string) > 0:	

		_string = _string[0]
	
		_spacemouse = avango.daemon.HIDInput()
		_spacemouse.station = avango.daemon.Station('device-spacemouse') # create a station to propagate the input events
		_spacemouse.device = _string

		# map incoming spacemouse events to station values
		_spacemouse.values[0] = "EV_ABS::ABS_X"   # trans X
		_spacemouse.values[1] = "EV_ABS::ABS_Z"   # trans Y
		_spacemouse.values[2] = "EV_ABS::ABS_Y"   # trans Z
		_spacemouse.values[3] = "EV_ABS::ABS_RX"  # rotate X
		_spacemouse.values[4] = "EV_ABS::ABS_RZ"  # rotate Y
		_spacemouse.values[5] = "EV_ABS::ABS_RY"  # rotate Z

		# buttons
		_spacemouse.buttons[0] = "EV_KEY::BTN_0" # left button
		_spacemouse.buttons[1] = "EV_KEY::BTN_1" # right button

		device_list.append(_spacemouse)
		print "SpaceMouse started at:", _string

	else:
		print "SpaceMouse NOT found !"


def init_old_spheron():

  _string = os.popen("./list-ev -s | grep \"BUWEIMAR RAPID DEVEL DEVICE\" | sed -e \'s/\"//g\'  | cut -d\" \" -f4").read()
  
  _string = _string.split()
  if len(_string) > 0:
  
    _string = _string[0]
    _spheron = avango.daemon.HIDInput()
    _spheron.station = avango.daemon.Station("device-old-spheron") # create a station to propagate the input events
    _spheron.device = _string
    
    # map incoming spheron events to station values
    _spheron.values[0] = "EV_ABS::ABS_X"   # trans X		
    _spheron.values[1] = "EV_ABS::ABS_Y"   # trans Y
    _spheron.values[2] = "EV_ABS::ABS_Z"   # trans Z
    _spheron.values[3] = "EV_ABS::ABS_RX"  # rotate X
    _spheron.values[4] = "EV_ABS::ABS_RY"  # rotate Y
    _spheron.values[5] = "EV_ABS::ABS_RZ"  # rotate Z
    
    device_list.append(_spheron)
    
    print 'Old Spheron started at:', _string
    
  else:
    print "Old Spheron NOT found !"
    
  _string = os.popen("./list-ev -s | grep \"PIXART USB OPTICAL MOUSE\" | sed -e \'s/\"//g\'  | cut -d\" \" -f4").read()
  
  _string = _string.split()
  if len(_string) > 0:
  
    _string = _string[0]
    
    _spheron_buttons = avango.daemon.HIDInput()
    _spheron_buttons.station = avango.daemon.Station("device-old-spheron-buttons") # create a station to propagate the input events
    _spheron_buttons.device = _string
    
    # map buttons
    _spheron_buttons.buttons[0] = "EV_KEY::BTN_RIGHT" # right
    _spheron_buttons.buttons[1] = "EV_KEY::BTN_LEFT" # left 
    _spheron_buttons.buttons[2] = "EV_KEY::BTN_MIDDLE" # middle
    
    device_list.append(_spheron_buttons)
    print 'Old Spheron Buttons started at:', _string
    
  else:
    print "Old Spheron ButTons NOT found !"


def init_mouse():

	mouse_name = os.popen("ls /dev/input/by-id | grep \"-event-mouse\" | sed -e \'s/\"//g\'  | cut -d\" \" -f4").read()

	mouse_name = mouse_name.split()
	if len(mouse_name) > 0:

		mouse_name = mouse_name[0]

		mouse = avango.daemon.HIDInput()
		mouse.station = avango.daemon.Station('gua-device-mouse')
		mouse.device = "/dev/input/by-id/" + mouse_name

		mouse.values[0] = "EV_REL::REL_X"
		mouse.values[1] = "EV_REL::REL_Y"

		mouse.buttons[0] = "EV_KEY::BTN_LEFT"
		mouse.buttons[1] = "EV_KEY::BTN_RIGHT"

		device_list.append(mouse)
		print "Mouse started at:", mouse_name

	else:
		print "Mouse NOT found !"


def init_keyboard():

	keyboard_name = os.popen("ls /dev/input/by-id | grep \"-event-kbd\" | sed -e \'s/\"//g\'  | cut -d\" \" -f4").read()

	keyboard_name = keyboard_name.split()

	for i, name in enumerate(keyboard_name):

		keyboard = avango.daemon.HIDInput()
		keyboard.station = avango.daemon.Station('gua-device-keyboard' + str(i))
		keyboard.device = "/dev/input/by-id/" + name


		keyboard.buttons[0] = "EV_KEY::KEY_Q"
		keyboard.buttons[1] = "EV_KEY::KEY_W"
		keyboard.buttons[2] = "EV_KEY::KEY_E"
		keyboard.buttons[3] = "EV_KEY::KEY_R"
		keyboard.buttons[4] = "EV_KEY::KEY_T"
		keyboard.buttons[5] = "EV_KEY::KEY_Z"
		keyboard.buttons[6] = "EV_KEY::KEY_U"
		keyboard.buttons[7] = "EV_KEY::KEY_I"
		keyboard.buttons[8] = "EV_KEY::KEY_O"
		keyboard.buttons[9] = "EV_KEY::KEY_P"
		keyboard.buttons[10] = "EV_KEY::KEY_A"
		keyboard.buttons[11] = "EV_KEY::KEY_S"
		keyboard.buttons[12] = "EV_KEY::KEY_D"
		keyboard.buttons[13] = "EV_KEY::KEY_F"
		keyboard.buttons[14] = "EV_KEY::KEY_G"
		keyboard.buttons[15] = "EV_KEY::KEY_H"
		keyboard.buttons[16] = "EV_KEY::KEY_J"
		keyboard.buttons[17] = "EV_KEY::KEY_K"
		keyboard.buttons[18] = "EV_KEY::KEY_L"
		keyboard.buttons[19] = "EV_KEY::KEY_Y"
		keyboard.buttons[20] = "EV_KEY::KEY_X"
		keyboard.buttons[21] = "EV_KEY::KEY_C"
		keyboard.buttons[22] = "EV_KEY::KEY_V"
		keyboard.buttons[23] = "EV_KEY::KEY_B"
		keyboard.buttons[24] = "EV_KEY::KEY_N"
		keyboard.buttons[25] = "EV_KEY::KEY_M"

		keyboard.buttons[26] = "EV_KEY::KEY_PAGEUP"
		keyboard.buttons[27] = "EV_KEY::KEY_PAGEDOWN"

		keyboard.buttons[28] = "EV_KEY::KEY_1"
		keyboard.buttons[29] = "EV_KEY::KEY_2"
		keyboard.buttons[30] = "EV_KEY::KEY_LEFT"
		keyboard.buttons[31] = "EV_KEY::KEY_RIGHT"


		device_list.append(keyboard)
		print "Keyboard " + str(i) + " started at:", name



def init_august_pointer():

  _string = os.popen("./list-ev -s | grep \"MOUSE USB MOUSE\" | sed -e \'s/\"//g\'  | cut -d\" \" -f4").read()
  
  _string = _string.split()
  if len(_string) > 0:
    _string = _string[0]
    
    _pointer = avango.daemon.HIDInput()
    _pointer.station = avango.daemon.Station('device-pointer1')
    _pointer.device = _string
    
    _pointer.buttons[0] = "EV_KEY::KEY_PAGEUP"
    _pointer.buttons[1] = "EV_KEY::KEY_PAGEDOWN"
    _pointer.buttons[2] = "EV_KEY::KEY_F5"


    device_list.append(_pointer)
    
    print "August Pointer started at:", _string
    
  else:
    print "August Pointer NOT found !"



def init_xinxin_pointer():

  _string = os.popen("./list-ev -s | grep \"XinXin Wireless Presenter\" | sed -e \'s/\"//g\'  | cut -d\" \" -f4").read()
  
  _string = _string.split()
  if len(_string) > 0:
    _string = _string[0]
    
    _pointer = avango.daemon.HIDInput()
    _pointer.station = avango.daemon.Station('device-pointer2')
    _pointer.device = _string
    
    _pointer.buttons[0] = "EV_KEY::KEY_PAGEUP"
    _pointer.buttons[1] = "EV_KEY::KEY_PAGEDOWN"


    device_list.append(_pointer)
    
    print "XinXin Pointer started at:", _string
    
  else:
    print "XinXin Pointer NOT found !"



def init_whitemouse_pointer():

  _string = os.popen("./list-ev -s | grep \"2.4G Receiver\" | sed -e \'s/\"//g\'  | cut -d\" \" -f4").read()
  
  _string = _string.split()
  if len(_string) > 0:
    _string = _string[0]
    
    _pointer = avango.daemon.HIDInput()
    _pointer.station = avango.daemon.Station('device-pointer2')
    _pointer.device = _string
    
    _pointer.buttons[0] = "EV_KEY::BTN_LEFT"
    _pointer.buttons[1] = "EV_KEY::BTN_RIGHT"


    device_list.append(_pointer)
    
    print "WhiteMouse Pointer started at:", _string
    
  else:
    print "WhiteMouse Pointer NOT found !"


def xbox_controller():

  _string = os.popen("./list-ev -s | grep \"Xbox 360 Wireless Receiver\" | sed -e \'s/\"//g\'  | cut -d\" \" -f4").read()
  
  _string = _string.split()
  if len(_string) > 0:
    _string = _string[0]
    
    _xbox = avango.daemon.HIDInput()
    _xbox.station = avango.daemon.Station('device-xbox')
    _xbox.device = _string
    
    _xbox.values[0] = "EV_ABS::ABS_X" # left joystick
    _xbox.values[1] = "EV_ABS::ABS_Y" # left joystick
    _xbox.values[2] = "EV_ABS::ABS_RX" # right joystick
    _xbox.values[3] = "EV_ABS::ABS_RY" # right joystick

    device_list.append(_xbox)
    
    print "XBox Controller " + " started at:", _string
    
  else:
    print "XBox Controller NOT found !"


device_list = []

init_pst_tracking()
#init_lcd_wall_tracking()
init_spacemouse()
xbox_controller()
init_august_pointer()
init_whitemouse_pointer()
init_old_spheron()
init_mouse()
init_keyboard()

avango.daemon.run(device_list)
