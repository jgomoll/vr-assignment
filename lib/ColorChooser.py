#!/usr/bin/python

import math
import pdb

import avango
import avango.gua
import avango.script

from lib.Object import InteractiveObject
from lib.Picker import *
from lib.Inputs import *
from lib.TextureLookup import *



class ColorChooser(InteractiveObject):

	loader = avango.gua.nodes.GeometryLoader()
	texture_lookup = TextureLookup("data/textures/detailed_color_wheel.jpg")

	def __init__(self, SCENE_GRAPH, MATRIX):
		color_chooser = self.loader.create_geometry_from_file("color_chooser", "data/objects/plane.obj", "ColorWheel", avango.gua.LoaderFlags.DEFAULTS | avango.gua.LoaderFlags.MAKE_PICKABLE | avango.gua.LoaderFlags.LOAD_MATERIALS)
		color_chooser.Transform.value = avango.gua.make_scale_mat(0.05, 0.05, 0.01) * avango.gua.make_rot_mat(270.0, 1,0,0)
		cube_geometry = self.loader.create_geometry_from_file("dummy_box", "data/objects/cube.obj", "ShadelessWhite", avango.gua.LoaderFlags.DEFAULTS)
		cube_geometry.Transform.value = avango.gua.make_trans_mat(0.0, 0.501, 0.0) * avango.gua.make_scale_mat(2.0, 1.0, 2.0)
		color_chooser.Children.value.append(cube_geometry)

		InteractiveObject.__init__(self, SCENE_GRAPH, MATRIX, color_chooser)
		InteractiveObject.geometric_manipulation_allowed(self, True, False)

		self.manipulated_once = False
		self.color_change_input = None
		self.raw_input = None
		self.frame_callback_trigger = avango.script.nodes.Update(Callback = self.frame_callback, Active = False)


	def enable_highlight(self, BOOL, INPUT):
		# do not highlight this object
		pass


	def register_input(self, RAW_INPUT):
		#if isinstance(RAW_INPUT, Picker) == False:		# does not work for some reason ...
		if RAW_INPUT.__class__.__name__ != "Picker":	# ... so use this expression instead
			return False

		if self.manipulated_once == False:
			manipulator_palette = self.transform.ManipulatorPaletteClass.value
			manipulator_palette.detach_from_palette_and_attach_to_scene_graph(self.transform)
			manipulator_palette.create_and_append_new_color_chooser()
			self.manipulated_once = True
			return InteractiveObject.try_register_input_on_self(self, RAW_INPUT)

		tex_coords = RAW_INPUT.mf_intersected_objects.value[0].TextureCoords.value
		distance_to_center = math.sqrt(pow(tex_coords[0] - 0.5, 2) + pow(tex_coords[1] - 0.5, 2))

		if distance_to_center > 0.5:
			return InteractiveObject.try_register_input_on_self(self, RAW_INPUT)
			pass
		else:
			if self.parent_object != None:
				self.raw_input = RAW_INPUT
				color_vec = self.texture_lookup.lookup_normalized(tex_coords[0], tex_coords[1], 90.0, True)
				self.color_change_input = ColorChangeInput(color_vec)			# TODO: instead of giving a color vector give the raw input in order to get an updated texture coordinate
				self.frame_callback_trigger.Active.value = True
				return self.parent_object.register_input(self.color_change_input)


	def unregister_input(self, INPUT):
		if self.color_change_input == None:
			InteractiveObject.unregister_input(self, INPUT)
		else:
			self.parent_object.unregister_input(self.color_change_input)
			self.frame_callback_trigger.Active.value = False
			self.color_change_input = None
			self.raw_input = None


	# Callbacks
	def frame_callback(self):
		tex_coords = self.raw_input.mf_intersected_objects.value[0].TextureCoords.value
		distance_to_center = math.sqrt(pow(tex_coords[0] - 0.5, 2) + pow(tex_coords[1] - 0.5, 2))

		if distance_to_center > 0.5:
			return	# do nothing in this case
		else:
			color_vec = self.texture_lookup.lookup_normalized(tex_coords[0], tex_coords[1], 90.0, True)
			self.color_change_input.change_color_vec(color_vec)


