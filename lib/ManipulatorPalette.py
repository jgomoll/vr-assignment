#!/usr/bin/python

import pdb

import avango
import avango.gua

from lib.ColorChooser import *
from lib.MaterialChooser import *



class ManipulatorPalette:

	def __init__(self, SCENE_GRAPH, NAVIGATION_TRANSFORM):
		self.scene_graph = SCENE_GRAPH
		self.navigation_transform = NAVIGATION_TRANSFORM

		self.transform = avango.gua.nodes.TransformNode(Name = "manipulator_palette", Transform = avango.gua.make_trans_mat(0.25, 0.1, 0.15) * avango.gua.make_rot_mat(15.0, 0,-1,0))
		self.navigation_transform.Children.value.append(self.transform)
		self.create_and_append_new_color_chooser()
		self.create_and_append_new_material_chooser()


	def create_and_append_new_color_chooser(self):
		color_chooser = ColorChooser(self.scene_graph, avango.gua.make_identity_mat())
		color_chooser.transform.add_and_init_field(avango.script.SFObject(), "ManipulatorPaletteClass", self)
		color_chooser.transform.Parent.value.Children.value.remove(color_chooser.transform)
		self.transform.Children.value.append(color_chooser.transform)


	def create_and_append_new_material_chooser(self):
		material_chooser = MaterialChooser(self.scene_graph, avango.gua.make_trans_mat(0.0, -0.1, -0.005))
		material_chooser.transform.add_and_init_field(avango.script.SFObject(), "ManipulatorPaletteClass", self)
		material_chooser.transform.Parent.value.Children.value.remove(material_chooser.transform)
		self.transform.Children.value.append(material_chooser.transform)


	def detach_from_palette_and_attach_to_scene_graph(self, OBJECT_TRANSFORM):
		matrix = self.navigation_transform.Transform.value * self.transform.Transform.value * OBJECT_TRANSFORM.Transform.value
		self.transform.Children.value.remove(OBJECT_TRANSFORM)
		self.scene_graph.Root.value.Children.value.append(OBJECT_TRANSFORM)
		OBJECT_TRANSFORM.Transform.value = matrix



