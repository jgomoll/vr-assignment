#!/usr/bin/python

from abc import ABCMeta
from abc import abstractmethod
import pdb

import avango
import avango.gua
import avango.script

from lib.OneHandedManipulation import *
from lib.Materials import Materials


class Manipulator:

	__metaclass__ = ABCMeta

	holding_manipulation_mode_object = None


	def __init__(self, HOLDING_MANIPULATION_MODE_OBJECT):
		self.holding_manipulation_mode_object = HOLDING_MANIPULATION_MODE_OBJECT


	@abstractmethod
	def start_manipulation(self, INPUT, MANIPULATED_OBJECT):
		# start manipulation with the object
		pass


	def stop_manipulation(self, INPUT, MANIPULATED_OBJECT):
		# stop manipulation with the object ...
		# ... and then call this parent method because the manipulator has to be destroyed (see next line)
		self.holding_manipulation_mode_object.destroy_manipulator()



class GeometricManipulator(Manipulator):

	one_handed_manipulation = None
	two_handed_manipulation = None


	def __init__(self, HOLDING_MANIPULATION_MODE_OBJECT):
		Manipulator.__init__(self, HOLDING_MANIPULATION_MODE_OBJECT)
		self.one_handed_manipulation = OneHandedManipulation()


	def start_manipulation(self, INPUT, MANIPULATED_OBJECT):
		self.one_handed_manipulation.my_constructor(MANIPULATED_OBJECT, INPUT)
		self.one_handed_manipulation.start_object_manipulation()


	def stop_manipulation(self, INPUT, MANIPULATED_OBJECT):
		self.one_handed_manipulation.stop_object_manipulation()
		Manipulator.stop_manipulation(self, INPUT, MANIPULATED_OBJECT)



class MaterialManipulator(Manipulator):

	def __init__(self, HOLDING_MANIPULATION_MODE_OBJECT):
		Manipulator.__init__(self, HOLDING_MANIPULATION_MODE_OBJECT)


	def start_manipulation(self, INPUT, MANIPULATED_OBJECT):
		self.input = INPUT
		self.apply_material_to_children(MANIPULATED_OBJECT)


	def stop_manipulation(self, INPUT, MANIPULATED_OBJECT):
		# do nothing here
		Manipulator.stop_manipulation(self, INPUT, MANIPULATED_OBJECT)


	def apply_material_to_children(self, PARENT):
		for object_ in PARENT.child_objects:
			if object_.__class__.__name__ == "Group":
				self.apply_material_to_children(object_)
			else:
				if object_.is_material_manipulation_allowed() == True:
					object_.geometry.Material.value = self.input.material



class ColorManipulator(Manipulator):

	def __init__(self, HOLDING_MANIPULATION_MODE_OBJECT):
		Manipulator.__init__(self, HOLDING_MANIPULATION_MODE_OBJECT)
		self.color_evaluation = avango.script.nodes.Update(Callback = self.evaluate, Active = False)


	def start_manipulation(self, INPUT, MANIPULATED_OBJECT):
		self.manipulating_input = INPUT
		self.manipulated_object = MANIPULATED_OBJECT
		self.color_evaluation.Active.value = True


	def stop_manipulation(self, INPUT, MANIPULATED_OBJECT):
		self.color_evaluation.Active.value = False
		Manipulator.stop_manipulation(self, INPUT, MANIPULATED_OBJECT)


	def evaluate(self):
		self.apply_color_to_children(self.manipulated_object)


	def apply_color_to_children(self, PARENT):
		for object_ in PARENT.child_objects:
			if object_.__class__.__name__ == "Group":
				self.apply_color_to_children(object_)
			else:
				if object_.is_color_manipulation_allowed() == True:
					material = object_.geometry.Material.value
					avango.gua.set_material_uniform(material, "diffuse_color", self.manipulating_input.color_vec)



