#!/usr/bin/python

import pdb

import avango
import avango.gua

from lib.Object import InteractiveObject
from lib.Inputs import MaterialChangeInput


class MaterialChangeBuzzer(InteractiveObject):

	def __init__(self, SCENE_GRAPH, MATRIX):
		loader = avango.gua.nodes.GeometryLoader()
		cube_geometry = loader.create_geometry_from_file("material_chooser_box", "data/objects/cube.obj", "SimplePhongGreen", avango.gua.LoaderFlags.DEFAULTS | avango.gua.LoaderFlags.MAKE_PICKABLE)
		cube_geometry.Transform.value = avango.gua.make_scale_mat(0.5, 0.5, 0.1)
		sphere_geometry = loader.create_geometry_from_file("material_chooser_button", "data/objects/sphere.obj", "SimplePhongRed", avango.gua.LoaderFlags.DEFAULTS | avango.gua.LoaderFlags.MAKE_PICKABLE)
		sphere_geometry.Transform.value = avango.gua.make_trans_mat(0.0, 0.0, 0.4) * avango.gua.make_scale_mat(0.4, 0.4, 0.4)
		cube_geometry.Children.value.append(sphere_geometry)

		InteractiveObject.__init__(self, SCENE_GRAPH, MATRIX, cube_geometry)
		InteractiveObject.geometric_manipulation_allowed(self, True, False)


	def enable_highlight(self, BOOL):
		# do not highlight this object
		pass


	def register_input(self, RAW_INPUT):
		hit_object = RAW_INPUT.mf_intersected_objects.value[0].Object.value.Name.value
		if hit_object != "material_chooser_button":
			InteractiveObject.redirect_register_input(self, RAW_INPUT)
		else:
			if self.parent_object != None:	# no parent -> no material change
				material_change_input = MaterialChangeInput()
				self.parent_object.register_input(material_change_input)
				self.parent_object.unregister_input(material_change_input)



