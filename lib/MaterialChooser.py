#!/usr/bin/python

import pdb

import avango
import avango.gua

from lib.Object import InteractiveObject
from lib.Inputs import MaterialChangeInput
from lib.Materials import *


class MaterialChooser(InteractiveObject):

	materials = MaterialsInstanceHolder().get_instance()

	def __init__(self, SCENE_GRAPH, MATRIX):
		self.loader = avango.gua.nodes.GeometryLoader()
		self.cube_geometry = self.loader.create_geometry_from_file("material_chooser_box", "data/objects/cube.obj", "ShadelessWhite", avango.gua.LoaderFlags.DEFAULTS | avango.gua.LoaderFlags.MAKE_PICKABLE)
		self.cube_geometry.Transform.value = avango.gua.make_scale_mat(0.1, 0.1, 0.01)
		InteractiveObject.__init__(self, SCENE_GRAPH, MATRIX, self.cube_geometry)
		InteractiveObject.geometric_manipulation_allowed(self, True, False)

		self.materials.register_material_chooser_for_update(self)

		self.manipulated_once = False
		self.previewed_materials = []
		self.preview_spheres = []
		self.add_material_buttons = []

		self.append_new_preview_spheres()
		self.init_add_material_buttons()
		self.update()


	def append_new_preview_spheres(self):
		available_materials = self.materials.get_available_materials()
		x = 0
		for material_group in available_materials:
			for material in material_group:
				if self.material_already_previewed(material) == False:
					self.append_material_sphere(material, x)
			x += 1


	def material_already_previewed(self, MATERIAL):
		for column in self.previewed_materials:
			for material in column:
				if material == MATERIAL:
					return True
		return False


	def append_material_sphere(self, MATERIAL, COLUMN):
		self.add_to_previewed_materials(MATERIAL, COLUMN)
		sphere = self.loader.create_geometry_from_file(MATERIAL, "data/objects/sphere.obj", MATERIAL, avango.gua.LoaderFlags.DEFAULTS | avango.gua.LoaderFlags.MAKE_PICKABLE)
		sphere.Transform.value = avango.gua.make_scale_mat(0.1, 0.1, 0.3)
		self.preview_spheres.append(sphere)
		self.cube_geometry.Children.value.append(sphere)


	def add_to_previewed_materials(self, MATERIAL, COLUMN):
		if COLUMN >= len(self.previewed_materials):
			self.previewed_materials.append([])
		column = self.previewed_materials[COLUMN]
		column.append(MATERIAL)


	def init_add_material_buttons(self):
		for column in range(len(self.previewed_materials)):
			button = self.loader.create_geometry_from_file("add_material_button_" + str(column), "data/objects/cube.obj", "PlusSign", avango.gua.LoaderFlags.DEFAULTS | avango.gua.LoaderFlags.MAKE_PICKABLE)
			button.Transform.value = avango.gua.make_scale_mat(0.2, 0.2, 0.3)
			self.add_material_buttons.append(button)
			self.cube_geometry.Children.value.append(button)


	def update(self):
		self.append_new_preview_spheres()
		self.resize_cube_geometry()
		self.update_preview_spheres()
		self.update_add_material_buttons()
			

	def resize_cube_geometry(self):
		self.max_count = 1
		for column in range(len(self.previewed_materials)):
			rows = len(self.previewed_materials[column]) + 1
			if rows > self.max_count:
				self.max_count = rows
		self.height = 0.025 * (self.max_count)
		self.cube_geometry.Transform.value = avango.gua.make_trans_mat(0.0, -self.height/2.0+0.05, 0.0) * avango.gua.make_scale_mat(0.1, self.height, 0.01)


	def update_preview_spheres(self):
		column = 0
		for material_group in self.previewed_materials:
			row = 0
			for material in material_group:
				sphere = self.get_sphere_object_from_preview_spheres(material)
				self.update_preview_sphere(sphere, column, row)
				row += 1
			column += 1


	def get_sphere_object_from_preview_spheres(self, NAME):
		for sphere in self.preview_spheres:
			if sphere.Name.value == NAME:
				return sphere
		return None


	def update_preview_sphere(self, SPHERE, COLUMN, ROW):
		scale = avango.gua.make_scale_mat(self.cube_geometry.Transform.value.get_scale())
		inverse_scale = avango.gua.make_inverse_mat(scale)
		x = -0.0625 + (COLUMN+1) * 0.025
		y = -((self.height/2.0) - ((2 * ROW + 1) / 80.0))
		SPHERE.Transform.value = inverse_scale * avango.gua.make_trans_mat(x, -y, 0.003) * avango.gua.make_scale_mat(0.01, 0.01, 0.003)


	def update_add_material_buttons(self):
		scale = avango.gua.make_scale_mat(self.cube_geometry.Transform.value.get_scale())
		inverse_scale = avango.gua.make_inverse_mat(scale)
		for column in range(len(self.previewed_materials)):
			row = len(self.previewed_materials[column])
			x = -0.0625 + (column+1) * 0.025
			y = -((self.height/2.0) - ((2 * row + 1) / 80.0))
			self.add_material_buttons[column].Transform.value = inverse_scale * avango.gua.make_trans_mat(x, -y, 0.004) * avango.gua.make_scale_mat(0.02, 0.02, 0.003)


	def enable_highlight(self, BOOL, INPUT):
		# do not highlight this object
		pass


	def register_input(self, RAW_INPUT):
		if self.manipulated_once == False:
			manipulator_palette = self.transform.ManipulatorPaletteClass.value
			manipulator_palette.detach_from_palette_and_attach_to_scene_graph(self.transform)
			manipulator_palette.create_and_append_new_material_chooser()
			self.manipulated_once = True
			return InteractiveObject.try_register_input_on_self(self, RAW_INPUT)

		hit_object = RAW_INPUT.mf_intersected_objects.value[0].Object.value.Name.value
		if hit_object == "material_chooser_box":
			InteractiveObject.try_register_input_on_self(self, RAW_INPUT)
			pass
		elif hit_object.startswith("add_material_button"):
			column = self.extract_category_from_button_name(hit_object)
			self.materials.show_new_material(column)
			self.update()
		else:
			if self.parent_object != None:	# no parent -> no material change
				material = hit_object
				material_change_input = MaterialChangeInput(material)
				self.parent_object.register_input(material_change_input)
				self.parent_object.unregister_input(material_change_input)
				return True


	def extract_category_from_button_name(self, BUTTON_NAME):
		begin_index = BUTTON_NAME.rfind("_") + 1
		return int(BUTTON_NAME[begin_index:])



