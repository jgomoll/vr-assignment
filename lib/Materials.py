#!/usr/bin/python

import os
import pdb
import shutil
import string
import random

import avango
import avango.gua



def singleton():
	if singleton.instance == None:
		singleton.instance = Materials()
	return singleton.instance

singleton.instance = None



class MaterialsInstanceHolder:

	def __init__(self):
		pass

	def get_instance(self):
		return singleton()



class Materials:

	material_groups = ["Shadeless", "SimplePhong", "EmitPhong", "CarPaint"]
	unshown_materials = {}
	shown_materials = {}

	material_choosers = []


	def __init__(self):
		avango.gua.load_shading_models_from("data/materials")
		avango.gua.load_materials_from("data/materials")
		self.initialize_start_materials()
		self.setup_visible_materials(3)


	def register_material_chooser_for_update(self, MATERIAL_CHOOSER):
		self.material_choosers.append(MATERIAL_CHOOSER)


	def initialize_start_materials(self):
		shutil.rmtree("/tmp/avango-materials", True)
		os.makedirs("/tmp/avango-materials")
		for i in range(len(self.material_groups)):
			for j in range(10):
				self.create_new_material(i)

		avango.gua.load_materials_from("/tmp/avango-materials")
		self.randomize_colors()


	def create_new_material(self, COLUMN):
		material_group = self.material_groups[COLUMN]
		name = self.get_random_string()
		shutil.copyfile("data/materials/" + material_group + ".gmd", "/tmp/avango-materials/" + name + ".gmd")
		if not material_group in self.unshown_materials.keys():
			self.unshown_materials[material_group] = [name]
		else:
			self.unshown_materials[material_group].append(name)


	def get_random_string(self):
		random_string = ''.join(random.choice(string.ascii_lowercase + string.digits) for i in range(12))
		return random_string


	def randomize_colors(self):
		for material_group in self.unshown_materials.values():
			for material in material_group:
				avango.gua.set_material_uniform(material, "diffuse_color", self.get_random_color())


	def get_random_color(self):
		return avango.gua.Vec3(random.random(), random.random(), random.random())


	def setup_visible_materials(self, COUNT):
		for material_group in self.material_groups:
			self.shown_materials[material_group] = []
			for i in range(COUNT):
				self.shown_materials[material_group].append(self.unshown_materials[material_group][0])
				self.unshown_materials[material_group].remove(self.unshown_materials[material_group][0])


	def get_default_material(self, MATERIAL_GROUP):
		return self.shown_materials[MATERIAL_GROUP][0]


	def get_available_materials(self):
		result = []
		for material_group in self.material_groups:
			result.append(self.shown_materials[material_group])
		return result


	def show_new_material(self, COLUMN):
		material_group = self.material_groups[COLUMN]
		if len(self.unshown_materials[material_group]) == 0:
			return

		self.shown_materials[material_group].append(self.unshown_materials[material_group][0])
		self.unshown_materials[material_group].remove(self.unshown_materials[material_group][0])
		for material_chooser in self.material_choosers:
			material_chooser.update()



