#!/usr/bin/python

import time
import pdb

import avango
import avango.gua

from lib.ObjectManipulationModes import *



class Object:
		
	def __init__(self, SCENE_GRAPH, MATRIX, GEOMETRY):
		self.geometry = GEOMETRY
		transform_name = "object_transform"
		if self.geometry != None:
			transform_name = self.geometry.Name.value + "_transform"
		self.transform = avango.gua.nodes.TransformNode(Name = transform_name, Transform = MATRIX)
		if self.geometry != None:
			self.transform.Children.value.append(self.geometry)

		self.scene_graph = SCENE_GRAPH
		self.scene_graph.Root.value.Children.value.append(self.transform)

		self.parent_object = None
		self.child_objects = []


	def append_object(self, OBJECT):
		_child_absolute_matrix 	= OBJECT.get_absolute_matrix() 	# actual absolute matrix of child object

		if OBJECT.parent_object != None:
			OBJECT.parent_object.remove_object(OBJECT)		# remove from former parent node
		else:
			OBJECT.transform.Parent.value.Children.value.remove(OBJECT.transform)		# remove from parental node (most likely root node)
		self.transform.Children.value.append(OBJECT.transform) 	# append child object's transformation node to this object's transformation node 
		self.child_objects.append(OBJECT)
		OBJECT.parent_object = self
		OBJECT.set_absolute_matrix(_child_absolute_matrix) 	# update relative transformation between parent and child object


	def remove_object(self, OBJECT):
		self.geometry.Children.value.remove(OBJECT.transform) 	# remove child object's transformation node from this object's transformation nod
		self.child_objects.remove(OBJECT)										   
		OBJECT.parent_object = None	


	def get_local_matrix(self):
		return self.transform.Transform.value


	def set_local_matrix(self, MATRIX):
		self.transform.Transform.value = MATRIX


	def get_absolute_matrix(self):
		if self.parent_object != None:
			_absolute_matrix = self.parent_object.get_absolute_matrix() * self.get_local_matrix() # accumulate all node matrices from this node up to root node
		else: 
			_absolute_matrix = self.get_local_matrix()
		return _absolute_matrix


	def set_absolute_matrix(self, MATRIX):
		if self.parent_object != None:
			_relative_matrix = avango.gua.make_inverse_mat(self.parent_object.get_absolute_matrix()) * MATRIX	# compute relative transformation to parent object ...
			self.set_local_matrix(_relative_matrix) 					# ... and apply it to local matrix
			#BoundingBox.update() 								# update matrix of bounding box
		else:
			self.set_local_matrix(MATRIX)



class InteractiveObject(Object):

	def __init__(self, SCENE_GRAPH, MATRIX, GEOMETRY):
		Object.__init__(self, SCENE_GRAPH, MATRIX, GEOMETRY)
		if self.geometry != None:
			self.geometry.add_and_init_field(avango.script.SFObject(), "InteractiveObjectClass", self)
		self.possible_manipulation_modes = []
		self.active_inputs = { }
		self.highlighting_input_object = None


	def geometric_manipulation_allowed(self, ALLOWED, MUST_HANDLE_INPUT=False):
		self.set_manipulation_mode_allowed(type(GeometricManipulationMode()), ALLOWED, MUST_HANDLE_INPUT)


	def material_manipulation_allowed(self, ALLOWED, MUST_HANDLE_INPUT=False):
		self.set_manipulation_mode_allowed(type(MaterialManipulationMode()), ALLOWED, MUST_HANDLE_INPUT)


	def color_manipulation_allowed(self, ALLOWED, MUST_HANDLE_INPUT=False):
		self.set_manipulation_mode_allowed(type(ColorManipulationMode()), ALLOWED, MUST_HANDLE_INPUT)


	def set_manipulation_mode_allowed(self, TYPE, ALLOWED, MUST_HANDLE_INPUT):
		manipulation_mode = self.get_manipulation_mode_from_list_by_type(TYPE)
		if ALLOWED == True and manipulation_mode == None:
				manipulation_mode = TYPE()
				manipulation_mode.set_must_handle_input(MUST_HANDLE_INPUT)
				self.possible_manipulation_modes.append(manipulation_mode)
		elif ALLOWED == False and manipulation_mode != None:
			self.possible_manipulation_modes.remove(manipulation_mode)


	def get_manipulation_mode_from_list_by_type(self, TYPE):
		for manipulation_mode in self.possible_manipulation_modes:
			if isinstance(manipulation_mode, TYPE):
				return manipulation_mode
		return None


	def is_color_manipulation_allowed(self):
		manipulation_mode = self.get_manipulation_mode_from_list_by_type(type(ColorManipulationMode()))
		return (manipulation_mode != None)


	def is_material_manipulation_allowed(self):
		manipulation_mode = self.get_manipulation_mode_from_list_by_type(type(MaterialManipulationMode()))
		return (manipulation_mode != None)


	def make_group_with(self, OBJECT):
		group = Group(self, OBJECT)
		self.reregister_geometric_input()
		OBJECT.reregister_geometric_input()


	def reregister_geometric_input(self):
		input_ = self.find_input_for_geometric_manipulation()
		self.unregister_input(input_)
		self.register_input(input_)


	def find_input_for_geometric_manipulation(self):
		for key, value in self.active_inputs.items():
			for manipulator in value:
				if isinstance(value[0], GeometricManipulator):
					return key


	def enable_highlight(self, BOOL, INPUT):
		#self.BoundingBox.enable(BOOL)
		if self.highlighting_input_object != None and self.highlighting_input_object != INPUT:
			return

		if BOOL == True:
			self.highlighting_input_object = INPUT
			self.material_before_highlight = self.geometry.Material.value
			self.geometry.Material.value = "Bright"
			avango.gua.set_material_uniform("Bright", "diffuse_color", avango.gua.Vec3(0.7, 0.7, 0.7))
		else:
			self.geometry.Material.value = self.material_before_highlight
			self.highlighting_input_object = None

	
	def register_input(self, INPUT):
		for manipulation_mode in self.possible_manipulation_modes:
			if manipulation_mode.can_handle_input(INPUT) == True and manipulation_mode.must_handle_input() == True:
				input_accepted = self.create_and_start_manipulation(INPUT, manipulation_mode)
				if input_accepted == True:
					return True

		# input does not have to be handled at this point, so try redirecting this input to parents
		return self.redirect_register_input(INPUT)


	def redirect_register_input(self, INPUT):
		if self.parent_object == None:		# highest node
			return self.try_register_input_on_self(INPUT)
		else:
			input_accepted = self.parent_object.redirect_register_input(INPUT)
			return (input_accepted == True) or self.try_register_input_on_self(INPUT)


	def try_register_input_on_self(self, INPUT):
		input_accepted = False
		for manipulation_mode in self.possible_manipulation_modes:
			if manipulation_mode.can_handle_input(INPUT):
				input_accepted = self.create_and_start_manipulation(INPUT, manipulation_mode) or (input_accepted == True)
		return input_accepted


	def create_and_start_manipulation(self, INPUT, MANIPULATION_MODE):
		manipulator = MANIPULATION_MODE.create_manipulator(self)
		if manipulator == None or self.manipulator_type_already_exists(manipulator):
			return False
		else:
			self.add_to_active_inputs(INPUT, manipulator)
			manipulator.start_manipulation(INPUT, self)
			return True


	def manipulator_type_already_exists(self, MANIPULATOR):
		for key, value in self.active_inputs.items():
			for manipulator in value:
				if type(manipulator) == type(MANIPULATOR):
					return True
		return False


	def add_to_active_inputs(self, INPUT, MANIPULATOR):
		active_manipulators_for_this_input = self.active_inputs.get(INPUT)
		if active_manipulators_for_this_input == None:
			active_manipulators_for_this_input = []
		active_manipulators_for_this_input.append(MANIPULATOR)
		self.active_inputs[INPUT] = active_manipulators_for_this_input


	def unregister_input(self, INPUT):
		if self.parent_object != None:
			self.parent_object.unregister_input(INPUT)

		active_manipulators = self.active_inputs.get(INPUT)
		if active_manipulators != None:
			for manipulator in active_manipulators:
				manipulator.stop_manipulation(INPUT, self)
				active_manipulators.remove(manipulator)
				manipulator = None
			del self.active_inputs[INPUT]



class Group(InteractiveObject):

	def __init__(self, OBJECT1, OBJECT2):
		InteractiveObject.__init__(self, OBJECT1.scene_graph, OBJECT1.transform.Transform.value, None)
		self.transform.Name.value = "group_transform"
		self.append_object(OBJECT1)
		self.append_object(OBJECT2)
		self.geometric_manipulation_allowed(True)
		self.material_manipulation_allowed(True, True)
		self.color_manipulation_allowed(True, True)



