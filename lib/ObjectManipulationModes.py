#!/usr/bin/python

from abc import ABCMeta
from abc import abstractmethod
import pdb

import avango
import avango.gua

from lib.Picker import *
from lib.Manipulators import *
from lib.Inputs import MaterialChangeInput
from lib.Inputs import ColorChangeInput


class ObjectManipulationMode:

	__metaclass__ = ABCMeta

	force_handle_input = False


	@abstractmethod
	def can_handle_input(self, RAW_INPUT):
		# check if the given INPUT can be handled and return True if so, or False otherwise
		pass


	def set_must_handle_input(self, MUST_HANDLE_INPUT):
		self.force_handle_input = MUST_HANDLE_INPUT


	def must_handle_input(self):
		return self.force_handle_input


	@abstractmethod
	def create_manipulator(self, MANIPULATED_OBJECT):
		# return the according manipulator
		pass


	@abstractmethod
	def destroy_manipulator(self):
		# do all necessary stuff to unregister the manipulator and make space for a new one
		pass



class GeometricManipulationMode(ObjectManipulationMode):

	Name = "GeometricManipulationMode"
	geometric_manipulator = None


	def can_handle_input(self, RAW_INPUT):
		if type(RAW_INPUT) == type(Picker()):
			return True
		else:
			return False


	def create_manipulator(self, MANIPULATED_OBJECT):
		if self.geometric_manipulator == None:
			# TODO check if time between first and second hit is inbetween threshold -> if not, return None
			#      currently it is just checked if an GeometricManipulator already exists or not (cf. the other manipulators)
			self.geometric_manipulator = GeometricManipulator(self)
		return self.geometric_manipulator


	def destroy_manipulator(self):
		self.geometric_manipulator = None



class MaterialManipulationMode(ObjectManipulationMode):

	Name = "MaterialManipulationMode"
	material_manipulator = None


	def can_handle_input(self, RAW_INPUT):
		if isinstance(RAW_INPUT, MaterialChangeInput):
			return True
		else:
			return False


	def create_manipulator(self, MANIPULATED_OBJECT):
		if self.material_manipulator == None:	# object can only have one material manipulator
			self.material_manipulator = MaterialManipulator(self)
			return self.material_manipulator
		else:
			return None


	def destroy_manipulator(self):
		self.material_manipulator = None



class ColorManipulationMode(ObjectManipulationMode):

	Name = "ColorManipulationMode"
	color_manipulator = None


	def can_handle_input(self, RAW_INPUT):
		if isinstance(RAW_INPUT, ColorChangeInput):
			return True
		else:
			return False


	def create_manipulator(self, MANIPULATED_OBJECT):
		if self.color_manipulator == None:	# object can only have one color manipulator
			self.color_manipulator = ColorManipulator(self)
			return self.color_manipulator
		else:
			return None


	def destroy_manipulator(self):
		self.color_manipulator = None



