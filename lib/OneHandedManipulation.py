#!/usr/bin/python

import pdb

import avango
import avango.gua
import avango.script
from avango.script import field_has_changed

from lib.SceneGroupConstruction import *



class OneHandedManipulation(avango.script.Script):

	def __init__(self):
		self.super(OneHandedManipulation).__init__()
		self.manipulated_object = None
		self.manipulating_input = None
		self.scene_group_construction = SceneGroupConstructionInstanceHolder().get_instance()


	def my_constructor(self, OBJECT, INPUT):
		self.manipulated_object = OBJECT
		self.manipulating_input = INPUT
		self.frame_callback_trigger = avango.script.nodes.Update(Callback = self.frame_callback, Active = False)


	def start_object_manipulation(self):
		self._object_mat = avango.gua.make_inverse_mat(self.manipulating_input.output_mat) * self.manipulated_object.get_absolute_matrix()
		self.scene_group_construction.register_grouping_candidate(self.manipulated_object)
		self.enable_frame_callback(True)

		
	def stop_object_manipulation(self):
		self.enable_frame_callback(False)
		self.scene_group_construction.unregister_grouping_candidate(self.manipulated_object)


	def enable_frame_callback(self, BOOL):
		self.frame_callback_trigger.Active.value = BOOL


	# Callbacks
	def frame_callback(self):
		ray_matrix = self.manipulating_input.output_mat
		self.manipulated_object.set_absolute_matrix(ray_matrix * self._object_mat)



