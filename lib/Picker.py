#!/usr/bin/python

import time
import pdb

import avango
import avango.script
from   avango.script import field_has_changed
import avango.gua

import examples_common.navigator
from   examples_common.GuaVE import GuaVE
import examples_common.device

from lib.TextureLookup import *


class Picker(avango.script.Script):

	SceneGraph = avango.gua.SFSceneGraph()
	Ray        = avango.gua.SFRayNode()
	Options    = avango.SFInt()
	Mask       = avango.SFString()
	
	mf_intersected_objects = avango.gua.MFPickResult()
	num_hit_targets	= avango.SFInt()
	sf_button = avango.SFBool()

	tracking_transmitter_offset = avango.gua.make_trans_mat(0.0,-0.26,0.9)


	def __init__(self):
		self.super(Picker).__init__()

		self.hovered_object = None
		self.old_hovered_object = None
		self.manipulated_object = None

		self.tracking_sensor = avango.daemon.nodes.DeviceSensor(DeviceService = avango.daemon.DeviceService())
		self.tracking_sensor.TransmitterOffset.value = self.tracking_transmitter_offset
		self.tracking_sensor.ReceiverOffset.value = avango.gua.make_identity_mat()

		self.device_sensor = avango.daemon.nodes.DeviceSensor(DeviceService = avango.daemon.DeviceService())

		self.always_evaluate(True)

		self.SceneGraph.value = avango.gua.nodes.SceneGraph()
		self.Ray.value  = avango.gua.nodes.RayNode()
		self.Options.value = avango.gua.PickingOptions.PICK_ONLY_FIRST_OBJECT | avango.gua.PickingOptions.GET_POSITIONS | avango.gua.PickingOptions.GET_TEXTURE_COORDS# | avango.gua.PickingOptions.GET_WORLD_POSITIONS
		self.Mask.value = "" 

		self.pick_ray = avango.gua.nodes.RayNode(Name = "pick_ray")
		self.pick_ray.Transform.value = avango.gua.make_scale_mat(0.1, 0.1, 10.0)
		self.Ray.value = self.pick_ray

		self.pick_ray_transform = avango.gua.nodes.TransformNode(Children = [self.pick_ray])
		self.pick_ray_transform.Transform.connect_from(self.tracking_sensor.Matrix)

		self.output_mat = avango.gua.make_identity_mat()

		self.dragging_trigger = avango.script.nodes.Update(Callback = self.dragging_callback, Active = False)


	def init_device(self, NUMBER):
		if NUMBER == 1:
			self.tracking_sensor.Station.value = "tracking-pst-augustpointer"
			self.device_sensor.Station.value = "device-pointer1"
			self.sf_button.connect_from(self.device_sensor.Button0)
		elif NUMBER == 2:
			self.tracking_sensor.Station.value = "tracking-pst-whitemousepointer"
			self.device_sensor.Station.value = "device-pointer2"
			self.sf_button.connect_from(self.device_sensor.Button0)


	def set_scene_graph(self, GRAPH):
		self.SceneGraph.value = GRAPH


	def connect_ray_to_navigation_transform(self, SF_NAVIGATION_TRANSFORM):
		SF_NAVIGATION_TRANSFORM.Children.value.append(self.pick_ray_transform)


	def evaluate(self):
		results = self.SceneGraph.value.ray_test(self.Ray.value,
				                             self.Options.value,
				                             self.Mask.value)
		self.mf_intersected_objects.value = results.value
		num_hit_targets = len(self.mf_intersected_objects.value)
		if self.num_hit_targets.value != num_hit_targets:
			self.num_hit_targets.value = num_hit_targets


	def start_object_manipulation(self, OBJECT):
		self.manipulated_object = OBJECT
		self.manipulated_object.register_input(self)
		

	def stop_object_manipulation(self):
		self.manipulated_object.unregister_input(self)
		self.manipulated_object = None


	def get_ray_mat(self):
		return self.Ray.value.WorldTransform.value


	def calculate_offset_mat(self):
		new_offset_mat = self.get_ray_mat() * self.offset_mat
		return new_offset_mat


	# Callbacks
	@field_has_changed(num_hit_targets)
	def highlight_callback(self):
		if self.manipulated_object != None:		# there already is an object highlighted, so ignore any highlighting changes
			return

		if len(self.mf_intersected_objects.value) > 0:
			geometry = self.mf_intersected_objects.value[0].Object.value

			if geometry.has_field("InteractiveObjectClass"):
				self.hovered_object = geometry.InteractiveObjectClass.value
				if self.old_hovered_object != None and self.hovered_object != self.old_hovered_object:
					self.old_hovered_object.enable_highlight(False, self)
				
				if self.hovered_object != self.old_hovered_object:
					self.hovered_object.enable_highlight(True, self)

				self.old_hovered_object = self.hovered_object

		else:
			if self.hovered_object != None:
				self.hovered_object.enable_highlight(False, self)
			if self.old_hovered_object != None:
				self.old_hovered_object.enable_highlight(False, self)
			
			self.hovered_object = None
			self.old_hovered_object = None


	@field_has_changed(sf_button)
 	def sf_button_changed(self):
		if self.sf_button.value == True and self.manipulated_object == None and self.hovered_object != None:
			local_intersection_point = self.mf_intersected_objects.value[0].Position.value
			self.offset_mat = avango.gua.make_trans_mat(local_intersection_point) * avango.gua.make_inverse_mat(self.get_ray_mat())
			self.output_mat = self.calculate_offset_mat()
			self.dragging_trigger.Active.value = True
			self.start_object_manipulation(self.hovered_object)

		elif self.sf_button.value == False and self.manipulated_object != None: # stop object manipulation
			self.dragging_trigger.Active.value = False
			#if self.hovered_object != self.manipulated_object:
			#	self.manipulated_object.enable_highlight(False, self)
			self.stop_object_manipulation()
			''' Hier liegt vermutlich der Bug, dass bestimmte Objekte nicht geunhighlighted werden '''
			if len(self.mf_intersected_objects.value) == 0 and self.hovered_object != None:
				self.hovered_object.enable_highlight(False, self)
			'''
			1. Linken Affen zuerst greifen
			2. Affe rechts daneben danach greifen
			3. rechten Affen so vor den ersten schieben, dass der erste Strahl den rechten Affen schneidet
			4. ersten Strahl los lassen -> erster Affe erhaelt Original-Material wieder
			5. zweiten Strahl los lassen -> zweiter Affe behaelt Highlighting
			'''


	# evaluated every frame when active
	def dragging_callback(self):
		self.output_mat = self.calculate_offset_mat()
		


