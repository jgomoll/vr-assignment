#!/usr/bin/python

import pdb

import avango
import avango.gua
import avango.script
from avango.script import field_has_changed



def singleton():
	if singleton.instance == None:
		singleton.instance = SceneGroupConstruction()
	return singleton.instance

singleton.instance = None

class SceneGroupConstructionInstanceHolder:

	def __init__(self):
		pass

	def get_instance(self):
		return singleton()



class SceneGroupConstruction(avango.script.Script):

	grouping_candidates = []
	distance_threshold = 0.2


	def __init__(self):
		self.super(SceneGroupConstruction).__init__()
		self.frame_callback_trigger = avango.script.nodes.Update(Callback = self.frame_callback, Active = False)


	def set_distance_threshold(self, THRESHOLD):
		self.distance_threshold = THRESHOLD


	def register_grouping_candidate(self, CANDIDATE):
		if not CANDIDATE in self.grouping_candidates:
			parent_already_registered = False
			for parent_candidate in self.grouping_candidates:
				parent_already_registered = self.check_for_parent(CANDIDATE, parent_candidate)
			if parent_already_registered == False:
				self.grouping_candidates.append(CANDIDATE)
		self.update_callback_trigger()


	def check_for_parent(self, CHILD, POSSIBLE_PARENT):
		parent = CHILD.parent_object
		if parent == None:
			return False
		elif parent == POSSIBLE_PARENT:
			return True
		else:
			return self.check_for_parent(parent, POSSIBLE_PARENT)


	def unregister_grouping_candidate(self, CANDIDATE):
		if CANDIDATE in self.grouping_candidates:
			self.grouping_candidates.remove(CANDIDATE)
		self.update_callback_trigger()


	def update_callback_trigger(self):
		if len(self.grouping_candidates) >= 2:
			self.frame_callback_trigger.Active.value = True
		else:
			self.frame_callback_trigger.Active.value = False


	def calculate_distance(self, OBJECT1, OBJECT2):
		translate1 = self.get_intersection_point(OBJECT1)
		translate2 = self.get_intersection_point(OBJECT2)
		diff_vec = translate1 - translate2
		distance = diff_vec.length()
		return distance


	def get_intersection_point(self, OBJECT):
		input_ = OBJECT.find_input_for_geometric_manipulation()
		intersected_element = input_.mf_intersected_objects.value[0]
		local_intersection_point = intersected_element.Position.value
		local_matrix = intersected_element.Object.value.Transform.value
		absolute_matrix = intersected_element.Object.value.InteractiveObjectClass.value.get_absolute_matrix()
		absolute_intersection_point = (absolute_matrix * local_matrix * avango.gua.make_trans_mat(local_intersection_point)).get_translate()
		return absolute_intersection_point


	def group_objects(self, OBJECT1, OBJECT2):
		self.unregister_grouping_candidate(OBJECT1)
		self.unregister_grouping_candidate(OBJECT2)
		OBJECT1.make_group_with(OBJECT2)


	# Callbacks
	def frame_callback(self):
		for i in range(len(self.grouping_candidates)):
			for j in range(len(self.grouping_candidates)):
				if j <= i:
					continue
				object1 = self.grouping_candidates[i]
				object2 = self.grouping_candidates[j]
				distance = self.calculate_distance(object1, object2)
				if distance < self.distance_threshold:
					self.group_objects(object1, object2)
					return



