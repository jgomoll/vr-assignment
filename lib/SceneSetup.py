#!/usr/bin/python

import	avango
import	avango.gua

from lib.Object import *
from lib.Picker import *
from lib.Materials import *



class SceneSetup:

	def __init__(self, LOADER, GRAPH):

		materials = MaterialsInstanceHolder().get_instance()

		''' LIGHT '''
		light = avango.gua.nodes.SpotLightNode(Name = "spot_light",
				                             Color = avango.gua.Color(1.0, 1.0, 1.0),
				                             Falloff = 0.009,
				                             Softness = 0.003,
				                             EnableShadows = True,
				                             ShadowMapSize = 4096,
				                             ShadowOffset = 0.0005)
		light.Transform.value = avango.gua.make_trans_mat(0.0, 40.0, 40.0) * \
				              avango.gua.make_rot_mat(-45.0, 1.0, 0.0, 0.0) * \
				              avango.gua.make_scale_mat(100, 100, 160)
		GRAPH.Root.value.Children.value.append(light)

		''' MONKEYS '''
		monkey1 = LOADER.create_geometry_from_file("monkey1", "data/objects/monkey.obj", materials.get_default_material("SimplePhong"), avango.gua.LoaderFlags.DEFAULTS | avango.gua.LoaderFlags.MAKE_PICKABLE)
		monkey1.Transform.value = avango.gua.make_scale_mat(0.1, 0.1, 0.1)
		monkey2 = LOADER.create_geometry_from_file("monkey2", "data/objects/monkey.obj", materials.get_default_material("EmitPhong"), avango.gua.LoaderFlags.DEFAULTS | avango.gua.LoaderFlags.MAKE_PICKABLE)
		monkey2.Transform.value = avango.gua.make_scale_mat(0.1, 0.1, 0.1)
		monkey3 = LOADER.create_geometry_from_file("monkey3", "data/objects/monkey.obj", materials.get_default_material("CarPaint"), avango.gua.LoaderFlags.DEFAULTS | avango.gua.LoaderFlags.MAKE_PICKABLE)
		monkey3.Transform.value = avango.gua.make_scale_mat(0.1, 0.1, 0.1)

		interactive_monkey1 = InteractiveObject(GRAPH, avango.gua.make_trans_mat(0.2, 0.1, 0.0) * avango.gua.make_rot_mat(90, 0, 1, 0) * avango.gua.make_scale_mat(0.5, 0.5, 0.5), monkey1)
		interactive_monkey1.geometric_manipulation_allowed(True)
		interactive_monkey1.material_manipulation_allowed(True, True)
		interactive_monkey1.color_manipulation_allowed(True, True)

		interactive_monkey2 = InteractiveObject(GRAPH, avango.gua.make_trans_mat(0.0, 0.1, 0.0), monkey2)
		interactive_monkey2.geometric_manipulation_allowed(True)
		interactive_monkey2.material_manipulation_allowed(True, True)
		interactive_monkey2.color_manipulation_allowed(True, True)
		#interactive_monkey2.make_group_with(interactive_monkey1)
			
		interactive_monkey3 = InteractiveObject(GRAPH, avango.gua.make_trans_mat(-0.3, 0.1, 0.0), monkey3)
		interactive_monkey3.geometric_manipulation_allowed(True)
		interactive_monkey3.material_manipulation_allowed(True, True)
		interactive_monkey3.color_manipulation_allowed(True, True)



class NonHighlightableObject(InteractiveObject):

	def __init__(self, SCENE_GRAPH, MATRIX, GEOMETRY):
		InteractiveObject.__init__(self, SCENE_GRAPH, MATRIX, GEOMETRY)

	def enable_highlight(self, BOOL):
		# don't allow highlighting
		pass



