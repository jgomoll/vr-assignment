#!/usr/bin/python

import avango
import avango.gua

from examples_common.GuaVE import GuaVE



class SimpleViewingSetup:

	Viewer = avango.gua.nodes.Viewer()
	Shell = GuaVE()

	# constructor
	def __init__(self, SCENEGRAPH, STEREO_MODE, HEADTRACKING_FLAG):

		# references
		self.SceneGraph = SCENEGRAPH

		# variables
		#self.window_size			= avango.gua.Vec2ui(2560, 1440) # in pixels
		self.window_size			= avango.gua.Vec2ui(1920, 1080) # in pixels											# Samsung TV
		#self.window_size			= avango.gua.Vec2ui(1280, 720) # in pixels
		#self.screen_size			= avango.gua.Vec2(0.595, 0.335) # in meter
		self.screen_size			= avango.gua.Vec2(1.235, 0.690) # in meter											# Samsung TV
		#self.screen_transform = avango.gua.make_trans_mat(0.0, 1.42, 0.0)
		#self.screen_transform = avango.gua.make_identity_mat()
		self.screen_transform = avango.gua.make_rot_mat(-32.0, 1, 0, 0)												# Samsung TV
		self.tracking_transmitter_offset = avango.gua.make_trans_mat(0.0,-0.26,0.9)						# Samsung TV

		### init viewing setup
		self.head_transform = avango.gua.nodes.TransformNode(Name = "head_transform")
		#self.head_transform.Transform.value = avango.gua.make_trans_mat(0.0, 1.42, 1.0)
		self.head_transform.Transform.value = avango.gua.make_trans_mat(0.0, 0.0, 0.6)

		if HEADTRACKING_FLAG == True:
			self.headtracking_sensor = avango.daemon.nodes.DeviceSensor(DeviceService = avango.daemon.DeviceService())
			self.headtracking_sensor.Station.value = "tracking-pst-head"
			self.headtracking_sensor.TransmitterOffset.value = self.tracking_transmitter_offset
			self.headtracking_sensor.ReceiverOffset.value = avango.gua.make_identity_mat()
			self.head_transform.Transform.connect_from(self.headtracking_sensor.Matrix)

		self.screen = avango.gua.nodes.ScreenNode(Name = "screen")
		self.screen.Width.value = self.screen_size.x
		self.screen.Height.value = self.screen_size.y
		self.screen.Transform.value = self.screen_transform

		self.navigation_transform = avango.gua.nodes.TransformNode(Name = "navigation_transform")
		self.navigation_transform.Children.value = [self.head_transform, self.screen]
		self.SceneGraph.Root.value.Children.value.append(self.navigation_transform)


		if STEREO_MODE == "mono":
			self.camera = avango.gua.nodes.Camera(SceneGraph = self.SceneGraph.Name.value)
			self.camera.LeftScreen.value = "/navigation_transform/screen"
			self.camera.RightScreen.value = "/navigation_transform/screen"
			self.camera.LeftEye.value = "/navigation_transform/head_transform"

			self.window = avango.gua.nodes.Window(Size = self.window_size, LeftResolution = self.window_size)

			self.pipeline = avango.gua.nodes.Pipeline(Window = self.window, LeftResolution = self.window_size)
			self.pipeline.Camera.value = self.camera
			self.pipeline.EnableStereo.value = False


		elif STEREO_MODE == "anaglyph":
			self.left_eye_transform = avango.gua.nodes.TransformNode(Name = "left_eye_transform")
			self.left_eye_transform.Transform.value = avango.gua.make_identity_mat()
			self.head_transform.Children.value.append(self.left_eye_transform)

			self.right_eye_transform = avango.gua.nodes.TransformNode(Name = "right_eye_transform")
			self.right_eye_transform.Transform.value = avango.gua.make_identity_mat()
			self.head_transform.Children.value.append(self.right_eye_transform)

			self.camera = avango.gua.nodes.Camera(SceneGraph = self.SceneGraph.Name.value)
			self.camera.LeftScreen.value = "/navigation_transform/screen"
			self.camera.RightScreen.value = "/navigation_transform/screen"
			self.camera.LeftEye.value = "/navigation_transform/head_transform/left_eye_transform"
			self.camera.RightEye.value = "/navigation_transform/head_transform/right_eye_transform"

			self.window = avango.gua.nodes.Window(Size = self.window_size, LeftResolution = self.window_size, RightResolution = self.window_size)
			self.window.StereoMode.value = avango.gua.StereoMode.ANAGLYPH_RED_CYAN

			self.pipeline = avango.gua.nodes.Pipeline(Window = self.window, LeftResolution = self.window_size, RightResolution = self.window_size)
			self.pipeline.Camera.value = self.camera
			self.pipeline.EnableStereo.value = True

			self.set_eye_distance(0.06)


		elif STEREO_MODE == "checkerboard":
			self.left_eye_transform = avango.gua.nodes.TransformNode(Name = "left_eye_transform")
			self.left_eye_transform.Transform.value = avango.gua.make_identity_mat()
			self.head_transform.Children.value.append(self.left_eye_transform)

			self.right_eye_transform = avango.gua.nodes.TransformNode(Name = "right_eye_transform")
			self.right_eye_transform.Transform.value = avango.gua.make_identity_mat()
			self.head_transform.Children.value.append(self.right_eye_transform)

			self.camera = avango.gua.nodes.Camera(SceneGraph = self.SceneGraph.Name.value)
			self.camera.LeftScreen.value = "/navigation_transform/screen"
			self.camera.RightScreen.value = "/navigation_transform/screen"
			self.camera.LeftEye.value = "/navigation_transform/head_transform/left_eye_transform"
			self.camera.RightEye.value = "/navigation_transform/head_transform/right_eye_transform"

			self.window = avango.gua.nodes.Window(Size = self.window_size, LeftResolution = self.window_size, RightResolution = self.window_size)
			self.window.StereoMode.value = avango.gua.StereoMode.CHECKERBOARD

			self.pipeline = avango.gua.nodes.Pipeline(Window = self.window, LeftResolution = self.window_size, RightResolution = self.window_size)
			self.pipeline.Camera.value = self.camera
			self.pipeline.EnableStereo.value = True

			self.set_eye_distance(0.06)


		#self.pipeline.EnableBloom.value = True
		#self.pipeline.BloomIntensity.value = 0.4
		#self.pipeline.BloomThreshold.value = 0.3
		#self.pipeline.BloomRadius.value = 10
		self.pipeline.AmbientColor.value = avango.gua.Color(0.5, 0.5, 0.5)

		self.pipeline.EnableBackfaceCulling.value = False
		
		self.pipeline.EnableSsao.value = True
		self.pipeline.SsaoRadius.value = 5.0
		self.pipeline.SsaoIntensity.value = 0.7

		avango.gua.create_texture("data/textures/skymap.jpg")
		self.pipeline.BackgroundTexture.value = "data/textures/skymap.jpg"

		self.pipeline.EnableFog.value = True
		self.pipeline.FogTexture.value = "data/textures/skymap.jpg"
		self.pipeline.FogStart.value = 11.0
		self.pipeline.FogEnd.value = 25.0

		self.pipeline.EnableVignette.value = True
		self.pipeline.VignetteColor.value = avango.gua.Color()
		self.pipeline.VignetteCoverage.value = 0.5
		self.pipeline.VignetteSoftness.value = 0.6

		self.pipeline.EnableFXAA.value = True
		self.pipeline.EnableFrustumCulling.value = True

		self.pipeline.EnableHDR.value = True
		self.pipeline.HDRKey.value = 3.0

		self.pipeline.EnableBloom.value = True
		self.pipeline.BloomRadius.value = 1.9
		self.pipeline.BloomThreshold.value = 0.9
		self.pipeline.BloomIntensity.value = 0.3
		
		self.pipeline.EnableFPSDisplay.value = True
		self.pipeline.EnableRayDisplay.value = True

		self.Viewer.Pipelines.value = [self.pipeline]
		self.Viewer.SceneGraphs.value = [self.SceneGraph]


	def set_eye_distance(self, VALUE):
		self.left_eye_transform.Transform.value = avango.gua.make_trans_mat(VALUE * -0.5, 0.0, 0.0)
		self.right_eye_transform.Transform.value = avango.gua.make_trans_mat(VALUE * 0.5, 0.0, 0.0)


	def run(self):
		self.Viewer.run()


	def append_node_to_navigation_transform(self, NODE):
		self.navigation_transform.Children.value.append(NODE)


	def list_variabels(self):
		self.Shell.list_variables()


	def connect_navigation_matrix(self, SF_MATRIX):
		self.navigation_transform.Transform.connect_from(SF_MATRIX)


	def get_rotation_center(self): # get relative head position (towards screen)
		return self.head_transform.Transform.value.get_translate()



