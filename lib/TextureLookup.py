#!/usr/bin/python

import Image
import math

import avango.gua



class TextureLookup:


	def __init__(self, PATH_TO_TEXTURE):
		self.image = Image.open(PATH_TO_TEXTURE)
		self.pixel_array = self.image.load()
		self.image_size = self.image.size


	def lookup_normalized(self, NORMALIZED_X, NORMALIZED_Y, ANGLE, FLIP_AXES):
		x = NORMALIZED_X * self.image_size[0]
		y = NORMALIZED_Y * self.image_size[1]
		return self.lookup(x, y, ANGLE, FLIP_AXES)


	def lookup(self, X, Y, ANGLE, FLIP_AXES):
		if FLIP_AXES == True:
			h = X
			X = Y
			Y = h
		x_rotated, y_rotated = self.get_rotated_coordinates(X, Y, ANGLE)
		values = self.pixel_array[x_rotated, y_rotated]
		return avango.gua.Vec3(values[0] / 255.0, values[1] / 255.0, values[2] / 255.0)


	def get_rotated_coordinates(self, X, Y, ANGLE):
		x_shifted = X - self.image_size[0] / 2
		y_shifted = Y - self.image_size[1] / 2
		x_rotated = x_shifted * math.cos(math.radians(ANGLE)) + y_shifted * math.sin(math.radians(ANGLE))
		y_rotated = x_shifted * -math.sin(math.radians(ANGLE)) + y_shifted * math.cos(math.radians(ANGLE))
		x_unshifted = x_rotated + self.image_size[0] / 2
		y_unshifted = y_rotated + self.image_size[1] / 2
		return [x_unshifted, y_unshifted]



