#!/usr/bin/python

import avango
import avango.script
from   avango.script import field_has_changed
import avango.gua

import examples_common.navigator
from   examples_common.GuaVE import GuaVE

import examples_common.device
import time

def setup_pipe():
	camera = avango.gua.nodes.Camera(LeftEye = "/eye",
                                   	RightEye = "/eye",
                                   	LeftScreen = "/eye/screen",
                                   	RightScreen = "/eye/screen",
                                  	SceneGraph = "scene")

	width = 1920;
	height = int(width * 9.0 / 16.0)
	size = avango.gua.Vec2ui(width, height)

	window = avango.gua.nodes.Window(Size = size,
		                         Title = "TwoHandedManipulation",
		                         LeftResolution = size)

	pipe = avango.gua.nodes.Pipeline(Camera = camera,
		                         Window = window,
		                         LeftResolution = size)


	avango.gua.create_texture("data/textures/skymap.jpg")

	pipe.OutputTextureName.value = "weimar_pipe"
	pipe.EnableSsao.value = True
	pipe.SsaoRadius.value = 5
	pipe.SsaoIntensity.value = 0.7

	pipe.EnableFog.value = True
	pipe.FogTexture.value = "data/textures/skymap.jpg"
	pipe.FogStart.value = 11.0
	pipe.FogEnd.value = 25.0

	pipe.BackgroundTexture.value = "data/textures/skymap.jpg"

	pipe.EnableVignette.value = True
	pipe.VignetteColor.value = avango.gua.Color()
	pipe.VignetteCoverage.value = 0.5
	pipe.VignetteSoftness.value = 0.6

	pipe.EnableFXAA.value = True
	pipe.EnableFrustumCulling.value = True

	pipe.EnableHDR.value = True
	pipe.HDRKey.value = 3.0

	pipe.EnableBloom.value = True
	pipe.BloomRadius.value = 1.9
	pipe.BloomThreshold.value = 0.9
	pipe.BloomIntensity.value = 0.3

	pipe.EnableFPSDisplay.value = True
	pipe.EnableRayDisplay.value = True


	return pipe
