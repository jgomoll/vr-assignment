#!/usr/bin/python

import avango
import avango.gua

from examples_common.navigator import *

from lib.SimpleViewingSetup import *
#from lib.MultiViewingSetup import *
from lib.SceneSetup	import *
from lib.Picker import *
from lib.ManipulatorPalette import *


def start():

	loader = avango.gua.nodes.GeometryLoader()
	graph = avango.gua.nodes.SceneGraph(Name = "scenegraph")

	scene_setup = SceneSetup(loader, graph)

	navigator = Navigator()
	navigator.OutTransform.value = avango.gua.make_identity_mat()
	navigator.StartLocation.value = avango.gua.Vec3(0.0, 0.0, 0.0)
	navigator.StartRotation.value = avango.gua.Vec2(0.0, 0.0)
	navigator.RotationSpeed.value = 0.2
	navigator.MotionSpeed.value = 0.04

	picker1 = Picker()
	picker1.init_device(1)
	picker1.set_scene_graph(graph)

	picker2 = Picker()
	picker2.init_device(2)
	picker2.set_scene_graph(graph)

	#viewing_setup = SimpleViewingSetup(graph, "mono", True)
	viewing_setup = SimpleViewingSetup(graph, "checkerboard", True)
	#viewing_setup = SimpleViewingSetup(graph, "anaglyph", True)
	#viewing_setup = MultiViewingSetup(graph)
	viewing_setup.connect_navigation_matrix(navigator.OutTransform)
	viewing_setup.append_node_to_navigation_transform(picker1.pick_ray_transform)
	viewing_setup.append_node_to_navigation_transform(picker2.pick_ray_transform)

	manipulator_palette = ManipulatorPalette(graph, viewing_setup.navigation_transform)

	''' # use pdb instead of GuaVE
	shell = GuaVE()
	shell.start(locals(), globals())
	'''
	viewing_setup.run()


if __name__ == '__main__':
	start()



